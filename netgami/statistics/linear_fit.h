#ifndef STATISTICS_LINEAR_FIT_H
#define STATISTICS_LINEAR_FIT_H

#include <vector>

#include "../classes.h"

using namespace std;

class linear_fit
{
    vector <double> x;
    vector <double> y;
 
public:
    
    struct fit
    {
        double slope;
        double intercept;
    };
    
    // Methods
    
    void add(double, double);
    fit fit();
};

#endif