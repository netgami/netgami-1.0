#ifndef STATISTICS_LINEAR_FIT_CPP
#define STATISTICS_LINEAR_FIT_CPP

#include "linear_fit.h"

// Methods

void linear_fit :: add(double x, double y)
{
    this->x.push_back(x);
    this->y.push_back(y);
}

struct linear_fit :: fit linear_fit :: fit()
{
    double sx = 0;
    double sx2 = 0;
    double sy = 0;
    double sxy = 0;
    
    for(unsigned int i=0; i<this->x.size(); i++)
    {
        sx += this->x[i];
        sx2 += this->x[i] * this->x[i];
        sy += this->y[i];
        sxy += this->x[i] * this->y[i];
    }
    
    double delta = this->x.size() * sx2 - sx * sx;

    struct fit fit;
    
    fit.intercept = (sx2 * sy - sx * sxy) / delta;
    fit.slope = (this->x.size() * sxy - sx * sy) / delta;
    
    return fit;
}

#endif