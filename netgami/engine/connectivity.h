#ifndef ENGINE_CONNECTIVITY_H
#define ENGINE_CONNECTIVITY_H

#include "../classes.h"
#include "../geometry/vec3.h"
#include "../springs/hooke.h"

namespace connectivity_extensions
{
    template <typename... pextensions> class iterator <pack <pextensions...>>
    {
    protected:
        
        unsigned int _connectivity;
        
        node_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> ** _links;
        vec * _cache;
        
    public:
        
        // Constructors
        
        iterator();
        
        // Methods
        
        void recache(node_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> *, vec);
        vec force();
        
        unsigned int connectivity();
        void connectivity(unsigned int);
        
        node_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> ** links();
    };
    
    template <typename... pextensions, typename... extensions> class iterator <pack <pextensions...>, stiffness, extensions...> : public iterator <pack <pextensions...>, extensions...>
    {
        double * _stiffness;
        
    public:
        
        // Constructors
        
        iterator();
        
        // Methods
        
        double * stiffness();
        void connectivity(unsigned int);
    };
};

template <typename... extensions> class connectivity : public connectivity_extensions :: iterator <connectivity_extensions :: pack <extensions...>, extensions...>
{
};

#endif