#ifndef ENGINE_ZONE_H
#define ENGINE_ZONE_H

#include "../classes.h"
#include "node.h"

namespace zone
{
    struct branch
    {
        double charge;
        vec positions;
    };
    
    struct leaf
    {
    };
};

#endif