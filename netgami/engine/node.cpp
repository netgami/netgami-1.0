#ifndef ENGINE_NODE_CPP
#define ENGINE_NODE_CPP

#include "node.h"
#include "../geometry/vec3.cpp"
#include "connectivity.cpp"

// base

// Methods

template <typename... cextensions> vec node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: position()
{
    return this->_position;
}

template <typename... cextensions> void node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: position(vec _position)
{
    this->_position = _position;
    this->_connectivity.recache(this, this->_position);
}

template <typename... cextensions> double node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: charge()
{
    return 1.;
}

template <typename... cextensions> void node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: charge(double)
{
}

template <typename... cextensions> bool node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: fixed()
{
    return false;
}

template <typename... cextensions> void node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>> :: fixed(bool)
{
}

// charge

// Methods

template <typename... cextensions, typename... extensions> double node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>, node_extensions :: charge, extensions...> :: charge()
{
    return this->_charge;
}

template <typename... cextensions, typename... extensions> void node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>, node_extensions :: charge, extensions...> :: charge(double _charge)
{
    this->_charge = _charge;
}

// fixed

// Methods

template <typename... cextensions, typename... extensions> bool node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>, node_extensions :: fixed, extensions...> :: fixed()
{
    return this->_fixed;
}

template <typename... cextensions, typename... extensions> void node_extensions :: iterator <connectivity_extensions :: pack <cextensions...>, node_extensions :: fixed, extensions...> :: fixed(bool _fixed)
{
    this->_fixed = _fixed;
}

#endif