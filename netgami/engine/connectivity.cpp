#ifndef ENGINE_CONNECTIVITY_CPP
#define ENGINE_CONNECTIVITY_CPP

#include "connectivity.h"
#include "../geometry/vec3.cpp"
#include "../springs/hooke.cpp"

// base

// Constructors

template <typename... pextensions> connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: iterator()
{
    this->_connectivity = 0;
    this->_links = NULL;
    this->_cache = NULL;
}

// Methods

template <typename... pextensions> void connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: recache(node_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> * node, vec position)
{
    for(unsigned int l = 0; l < this->_connectivity; l++)
        for(unsigned int i=0; i<this->_links[l]->_connectivity._connectivity; i++)
            if(this->_links[l]->_connectivity._links[i] == node)
            {
                this->_links[l]->_connectivity._cache[i] = position;
                break;
            }
}

template <typename... pextensions> vec connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: force()
{
    return vec :: null; // PLEASE IMPLEMENT!!
}

template <typename... pextensions> unsigned int connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: connectivity()
{
    return this->_connectivity;
}

template <typename... pextensions> void connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: connectivity(unsigned int _connectivity)
{
    this->_connectivity = _connectivity;
    
    if(this->_links)
        delete [] this->_links;
    
    if(this->_cache)
        delete [] this->_cache;
    
    this->_links = new node_extensions :: iterator <connectivity_extensions :: pack <>> * [this->_connectivity];
    this->_cache = new vec[this->_connectivity];
}

template <typename... pextensions> node_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> ** connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>> :: links()
{
    return this->_links;
}

// stiffness

// Constructors

template <typename... pextensions, typename... extensions> connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>, connectivity_extensions :: stiffness, extensions...> :: iterator() : connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>, extensions...> :: iterator()
{
    this->_stiffness = NULL;
}

// Methods

template <typename... pextensions, typename... extensions> double * connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>, connectivity_extensions :: stiffness, extensions...> :: stiffness()
{
    return this->_stiffness;
}

template <typename... pextensions, typename... extensions> void connectivity_extensions :: iterator <connectivity_extensions :: pack <pextensions...>, connectivity_extensions :: stiffness, extensions...> :: connectivity(unsigned int _connectivity)
{
    this->iterator <extensions...> :: connectivity(_connectivity);
    
    if(this->_stiffness)
        delete [] this->_stiffness;
    
    this->_stiffness = new double[this->_connectivity];
}

#endif