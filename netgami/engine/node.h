#ifndef ENGINE_NODE_H
#define ENGINE_NODE_H

#include "../classes.h"
#include "../geometry/vec3.h"
#include "connectivity.h"

namespace node_extensions
{
    template <typename... cextensions> class iterator <connectivity_extensions :: pack <cextensions...>>
    {
        vec _position;
        connectivity <cextensions...> _connectivity;
        
    public:
        
        vec position();
        void position(vec);
        
        double charge();
        void charge(double);
        
        bool fixed();
        void fixed(bool);
        
        template <typename... fextensions> friend class connectivity_extensions :: iterator;
    };
    
    template <typename... cextensions, typename... extensions> class iterator <connectivity_extensions :: pack <cextensions...>, charge, extensions...> : public iterator <connectivity_extensions :: pack <cextensions...>, extensions...>
    {
        double _charge;
        
    public:
        
        double charge();
        void charge(double);
    };
    
    template <typename... cextensions, typename... extensions> class iterator <connectivity_extensions :: pack <cextensions...>, fixed, extensions...> : public iterator <connectivity_extensions :: pack <cextensions...>, extensions...>
    {
        bool _fixed;
        
    public:
        
        bool fixed();
        void fixed(bool);
    };
    
    template <typename... cextensions, typename cextension, typename... extensions> class iterator <connectivity_extensions :: pack <cextensions...>, cextension, extensions...> : public iterator <connectivity_extensions :: pack <cextensions..., cextension>, extensions...>
    {
    };
};

template <typename... extensions> class node : public node_extensions :: iterator <connectivity_extensions :: pack <>, extensions...>
{
};

#endif