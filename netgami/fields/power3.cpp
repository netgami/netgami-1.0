#ifndef FIELDS_POWER3_CPP
#define FIELDS_POWER3_CPP

#include "power3.h"
#include "../geometry/vec3.cpp"

// Methods

vec3 fields :: power3 :: field(vec3 position, double charge)
{
    double distance = !position;
    return (fields :: power3 :: permittivity * charge * position) / (pow(distance, fields :: power3 :: power + 1.));
}

double fields :: power3 :: potential(vec3 position, double charge)
{
    return (fields :: power3 :: permittivity * charge) / (pow(!position, fields :: power3 :: power - 1.) * (fields :: power3 :: power - 1.));
}

#endif