#ifndef FIELDS_POWER3_H
#define FIELDS_POWER3_H

#include "../classes.h"
#include "../geometry/vec3.h"

namespace fields
{
    class power3
    {
    public:
        
        static double permittivity;
        static double power;
        
        // Methods
        
        static vec3 field(vec3, double = 1.0);
        static double potential(vec3, double = 1.0);
    };
};

double fields :: power3 :: permittivity = 1.0;
double fields :: power3 :: power = 2.0;

#endif