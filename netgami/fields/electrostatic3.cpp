#ifndef FIELDS_ELECTROSTATIC3_CPP
#define FIELDS_ELECTROSTATIC3_CPP

#include "electrostatic3.h"
#include "../geometry/vec3.cpp"

// Methods

vec3 fields :: electrostatic3 :: field(vec3 position, double charge)
{
    double distance = !position;
    return (fields :: electrostatic3 :: permittivity * charge * position) / (distance * distance * distance);
}

double fields :: electrostatic3 :: potential(vec3 position, double charge)
{
    return (fields :: electrostatic3 :: permittivity * charge) / (!position);
}

#endif