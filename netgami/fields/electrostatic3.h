#ifndef FIELDS_ELECTROSTATIC3_H
#define FIELDS_ELECTROSTATIC3_H

#include "../classes.h"
#include "../geometry/vec3.h"

namespace fields
{
    class electrostatic3
    {
    public:
    
        static double permittivity;
        
        // Methods
        
        static vec3 field(vec3, double = 1.0);
        static double potential(vec3, double = 1.0);
    };
};

double fields :: electrostatic3 :: permittivity = 1.0;

#endif