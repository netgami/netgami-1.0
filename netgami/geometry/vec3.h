#ifndef GEOMETRY_VEC3_H
#define GEOMETRY_VEC3_H

#include <iostream>
#include <math.h>
#include <random>

#include "../classes.h"

using namespace std;

struct vec3
{
    double x;
    double y;
    double z;
    
    // Constructors
    
    vec3();
    vec3(double, double, double);
    vec3(const vec3 &);
    
    // Operators
    
    vec3 operator + (vec3) const;
    void operator += (vec3);
    
    vec3 operator - (vec3) const;
    void operator -= (vec3);
    
    vec3 operator - () const;
    
    vec3 operator * (double) const;
    void operator *= (double);
    
    double operator * (vec3) const;
    
    vec3 operator / (double) const;
    void operator /= (double);
    
    double operator ~ () const;
    double operator ! () const;
    
    void operator = (vec3);
    
    // Constants
    
    static vec3 null;
    
    // Generators
    
    class random
    {
        static __thread default_random_engine * randomizer;

    public:
        
        // Methods
        
        static void enable();
        static void disable();
        
        static vec3 uniform_box(double);
        static vec3 gaussian_box(double, double);
        static vec3 uniform_polar(double);
    };
    
    static random random;
};

vec3 operator * (double, vec3);

// Ostream

ostream & operator << (ostream &, vec3);

// Static declarations

vec3 vec3 :: null(0, 0, 0);
__thread default_random_engine * vec3 :: random :: randomizer;

#endif