#ifndef GEOMETRY_VEC3_CPP
#define GEOMETRY_VEC3_CPP

#include "vec3.h"

// Constructors

vec3 :: vec3()
{
}

vec3 :: vec3(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

vec3 :: vec3(const vec3 & v)
{
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
}

// Operators

vec3 vec3 :: operator + (vec3 v) const
{
    return vec3(this->x + v.x, this->y + v.y, this->z + v.z);
}

void vec3 :: operator += (vec3 v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
}

vec3 vec3 :: operator - (vec3 v) const
{
    return vec3(this->x - v.x, this->y - v.y, this->z - v.z);
}

void vec3 :: operator -= (vec3 v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
}

vec3 vec3 :: operator - () const
{
    return vec3(-this->x, -this->y, -this->z);
}

vec3 vec3 :: operator * (double l) const
{
    return vec3(l * this->x, l * this->y, l * this->z);
}

vec3 operator * (double l, vec3 v)
{
    return vec3(l * v.x, l * v.y, l * v.z);
}

void vec3 :: operator *= (double l)
{
    this->x *= l;
    this->y *= l;
    this->z *= l;
}

double vec3 :: operator * (vec3 v) const
{
    return this->x * v.x + this->y * v.y + this->z * v.z;
}

vec3 vec3 :: operator / (double l) const
{
    return vec3(this->x / l, this->y / l, this->z / l);
}

void vec3 :: operator /= (double l)
{
    this->x /= l;
    this->y /= l;
    this->z /= l;
}

double vec3 :: operator ~ () const
{
    return this->x * this->x + this->y * this->y + this->z * this->z;
}

double vec3 :: operator ! () const
{
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

void vec3 :: operator = (vec3 v)
{
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
}

// Ostream

ostream & operator << (ostream & out, vec3 v)
{
    out<<"("<<v.x<<", "<<v.y<<", "<<v.z<<")";
    return out;
}

// Generators

void vec3 :: random :: enable()
{
    vec3 :: random :: randomizer = new default_random_engine();
}

void vec3 :: random :: disable()
{
    delete vec3 :: random :: randomizer;
}

vec3 vec3 :: random :: uniform_box(double edge)
{
    uniform_real_distribution <double> distribution(-edge / 2., edge / 2.);
    
    return vec3(distribution(*(vec3 :: random :: randomizer)), distribution(*(vec3 :: random :: randomizer)), distribution(*(vec3 :: random :: randomizer)));
}

vec3 vec3 :: random :: gaussian_box(double edge, double sigma)
{
    normal_distribution <double> distribution(0.0, sigma);
    
    vec3 v;
    
    do
        v.x = distribution(*(vec3 :: random :: randomizer));
    while(v.x < -edge / 2. || v.x > edge / 2.);
    
    do
        v.y = distribution(*(vec3 :: random :: randomizer));
    while(v.y < -edge / 2. || v.y > edge / 2.);
    
    do
        v.z = distribution(*(vec3 :: random :: randomizer));
    while(v.z < -edge / 2. || v.z > edge / 2.);
    
    return v;
}

vec3 vec3 :: random :: uniform_polar(double radius)
{
    uniform_real_distribution <double> distribution(0.0, 1.0);

    double theta = acos(2.0 * distribution(*(vec3 :: random :: randomizer)) - 1.0);
    double phi = 2.0 * M_PI * distribution(*(vec3 :: random :: randomizer));
    
    return radius * vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
}

#endif