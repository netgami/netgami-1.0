#ifndef PLOT_PLOTTER_H
#define PLOT_PLOTTER_H

#ifdef __APPLE__
#include <Python/Python.h>
#endif
#ifdef __linux__
#include <python2.7/Python.h>
#endif

#include <thread>
#include <semaphore.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>

#include "../classes.h"
#include "line.h"

enum plot_type {LINEAR, LOGLOG, SEMILOGX, SEMILOGY};

enum legend_location {BEST, UPPER_RIGHT, UPPER_LEFT, LOWER_LEFT, LOWER_RIGHT, RIGHT, CENTER_LEFT, CENTER_RIGHT, LOWER_CENTER, UPPER_CENTER, CENTER};

using namespace std;

class plotter
{
    thread service_thread;
    
    struct
    {
        PyObject * module;
        PyObject * dictionary;
    } module;
    
    struct
    {
        sem_t * submit;
        sem_t * process;
        sem_t * complete;
    } semaphores;
    
    struct
    {
        bool kill;
    } thread_flags;
    
    struct
    {
        struct
        {
            line_plot * line_plot;
        } plot;
        
        struct
        {
            const char * path;
            plot_type plot_type;
            legend_location legend_location;
        } properties;
    } jobs;
    
public:
    
    // Constructors
    
    plotter();
    
    // Destructor
    
    ~plotter();
  
    // Methods
    
    void plot(const char *, line_plot &, enum plot_type = LINEAR, enum legend_location = UPPER_RIGHT);
    
private:
    
    // Private methods
    
    void line_plot();
    
    // Services
    
    void service();
};

plotter plotter;

#endif
