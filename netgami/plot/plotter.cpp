#ifndef PLOTTER_CPP
#define PLOTTER_CPP

#include "plotter.h"
#include "line.cpp"

// Constructors

plotter :: plotter()
{
    signal(SIGCHLD, SIG_IGN);
    
    sem_unlink("/netgami_plotter_submit");
    sem_unlink("/netgami_plotter_process");
    sem_unlink("/netgami_plotter_complete");
    
    this->semaphores.submit = sem_open("/netgami_plotter_submit", O_CREAT, 0755, 0);
    this->semaphores.process = sem_open("/netgami_plotter_process", O_CREAT, 0755, 0);
    this->semaphores.complete = sem_open("/netgami_plotter_complete", O_CREAT, 0755, 0);
    
    
    this->thread_flags.kill = false;
    
    this->jobs.plot.line_plot = NULL;
    
    this->service_thread = thread(&plotter :: service, this);
}

// Destructor

plotter :: ~plotter()
{
    sem_wait(this->semaphores.submit);
    this->thread_flags.kill = true;
    sem_post(this->semaphores.process);
    
    this->service_thread.join();
}

// Methods

void plotter :: plot(const char * path, class line_plot & plot, enum plot_type plot_type, enum legend_location legend_location)
{
    sem_wait(this->semaphores.submit);
    
    this->jobs.plot.line_plot = &plot;
    this->jobs.properties.path = path;
    this->jobs.properties.plot_type = plot_type;
    this->jobs.properties.legend_location = legend_location;
    
    sem_post(this->semaphores.process);
    sem_wait(this->semaphores.complete);
    sem_post(this->semaphores.submit);
}

// Private methods

void plotter :: line_plot()
{
    class line_plot & plot = *(this->jobs.plot.line_plot);
    
    PyObject * title = PyString_FromString(plot.descriptions.title);
    PyObject * label_x = PyString_FromString(plot.descriptions.axes.x);
    PyObject * label_y = PyString_FromString(plot.descriptions.axes.y);
    
    PyObject * title_func = PyDict_GetItemString(this->module.dictionary, "title");
    PyObject * label_x_func = PyDict_GetItemString(this->module.dictionary, "xlabel");
    PyObject * label_y_func = PyDict_GetItemString(this->module.dictionary, "ylabel");
    
    PyObject * title_func_args = PyTuple_New(1);
    PyTuple_SetItem(title_func_args, 0, title);
    
    PyObject * label_x_func_args = PyTuple_New(1);
    PyTuple_SetItem(label_x_func_args, 0, label_x);
    
    PyObject * label_y_func_args = PyTuple_New(1);
    PyTuple_SetItem(label_y_func_args, 0, label_y);
    
    PyObject_CallObject(title_func, title_func_args);
    PyObject_CallObject(label_x_func, label_x_func_args);
    PyObject_CallObject(label_y_func, label_y_func_args);
    
    Py_DECREF(title_func_args);
    Py_DECREF(label_x_func_args);
    Py_DECREF(label_y_func_args);
    
    for(unsigned int i=0; i<plot.lines.size(); i++)
    {
        plot.lines[i].sort();
        
        PyObject * x = PyList_New(0);
        PyObject * y = PyList_New(0);
        
        for(unsigned int j=0; j<plot.lines[i].x.size(); j++)
        {
            PyObject * x_item = PyFloat_FromDouble(plot.lines[i].x[j]);
            PyObject * y_item = PyFloat_FromDouble(plot.lines[i].y[j]);
            
            PyList_Append(x, x_item);
            PyList_Append(y, y_item);
            
            Py_DECREF(x_item);
            Py_DECREF(y_item);
        }
        
        const char * plot_functions[] = {"plot", "loglog", "semilogx", "semilogy"};
        
        PyObject * plot_func = PyDict_GetItemString(this->module.dictionary, plot_functions[this->jobs.properties.plot_type]);
        
        PyObject * plot_func_args = PyTuple_New(2);
        PyTuple_SetItem(plot_func_args, 0, x);
        PyTuple_SetItem(plot_func_args, 1, y);
        
        PyObject * plot_func_named_args = PyDict_New();
        PyObject * line_description = PyString_FromString(plot.lines[i].description);
        
        PyDict_SetItemString(plot_func_named_args, "label", line_description);
        
        PyObject_Call(plot_func, plot_func_args, plot_func_named_args);
        
        Py_DECREF(plot_func_args);
        Py_DECREF(line_description);
        Py_DECREF(plot_func_named_args);
    }
    
    PyObject * legend_func = PyDict_GetItemString(this->module.dictionary, "legend");
    
    PyObject * legend_func_args = PyTuple_New(0);
    
    PyObject * legend_func_named_args = PyDict_New();
    PyObject * legend_location = PyInt_FromLong(this->jobs.properties.legend_location);
    PyDict_SetItemString(legend_func_named_args, "loc", legend_location);
    
    PyObject_Call(legend_func, legend_func_args, legend_func_named_args);
    
    Py_DECREF(legend_func_args);
    Py_DECREF(legend_location);
    Py_DECREF(legend_func_named_args);
    
    PyObject * savefig_func = PyDict_GetItemString(this->module.dictionary, "savefig");
    
    PyObject * py_path = PyString_FromString(this->jobs.properties.path);
    PyObject * savefig_func_args = PyTuple_New(1);
    PyTuple_SetItem(savefig_func_args, 0, py_path);
    
    PyObject_CallObject(savefig_func, savefig_func_args);
    
    PyObject * clf_func = PyDict_GetItemString(this->module.dictionary, "clf");
    PyObject_CallObject(clf_func, NULL);
}

// Services

void plotter :: service()
{
    Py_Initialize();
    
    PyObject * module_name = PyString_FromString("matplotlib.pyplot");
    
    this->module.module = PyImport_Import(module_name);
    this->module.dictionary = PyModule_GetDict(this->module.module);
    
    Py_DECREF(module_name);
    
    sem_post(this->semaphores.submit);
    
    while(true)
    {
        sem_wait(this->semaphores.process);
        
        if(this->thread_flags.kill)
        {
            Py_Finalize();
            return;
        }
        
        if(this->jobs.plot.line_plot)
        {
            this->line_plot();
            this->jobs.plot.line_plot = NULL;
        }
        
        sem_post(this->semaphores.complete);
    }
}

#endif