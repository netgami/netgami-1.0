#ifndef PLOT_LINE_H
#define PLOT_LINE_H

#include <algorithm>
#include <vector>
#include <fstream>

#include "../classes.h"
#include "plotter.h"

using namespace std;

class line_plot
{
    struct line
    {
        const char * description;
        
        vector <double> x;
        vector <double> y;
        
        // Constructors
        
        line(const char *);
        
        // Methods
        
        void add(double, double);
        void sort();
        
        void save(const char *);
        void load(const char *);
    };
    
    struct
    {
        const char * title;
        
        struct
        {
            const char * x;
            const char * y;
        } axes;
        
    } descriptions;
    
public:
    
    vector <line> lines;
    
    // Constructors
    
    line_plot(const char *, const char *, const char *, vector <const char *>);
    
    // Methods
    
    void render(const char *);
    
    friend class plotter;
};

#endif