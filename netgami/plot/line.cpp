#ifndef PLOT_LINE_CPP
#define PLOT_LINE_CPP

#include "line.h"
#include "plotter.cpp"

// line

// Constructors

line_plot :: line :: line(const char * description)
{
    this->description = description;
}

// Methods

void line_plot :: line :: add(double x, double y)
{
    this->x.push_back(x);
    this->y.push_back(y);
}

void line_plot :: line :: sort()
{
    vector <unsigned int> indexes;

    for(unsigned int i=0; i<this->x.size(); i++)
        indexes.push_back(i);
    
    std::sort(indexes.begin(), indexes.end(), [=](const int & a, const int & b)
    {
        return this->x[a] < this->x[b];
    });
    
    vector <double> new_x;
    vector <double> new_y;
    
    for(unsigned int i=0; i<indexes.size(); i++)
    {
        new_x.push_back(this->x[indexes[i]]);
        new_y.push_back(this->y[indexes[i]]);
    }
    
    this->x = new_x;
    this->y = new_y;
}

void line_plot :: line :: save(const char * path)
{
    this->sort();
    
    ofstream handle(path, ios::binary | ios::out);
    
    for(unsigned int i=0; i<this->x.size(); i++)
    {
        handle.write((char *) &(this->x[i]), sizeof(double));
        handle.write((char *) &(this->y[i]), sizeof(double));
    }
    
    handle.close();
}

void line_plot :: line :: load(const char * path)
{
    this->x.clear();
    this->y.clear();
    
    ifstream handle(path, ios::binary | ios::in);
    
    handle.seekg(0, ios::end);
    unsigned long int size = handle.tellg();
    handle.seekg(0, ios::beg);
    
    for(unsigned int i = 0; i < size / (2 * sizeof(double)); i++)
    {
        double x;
        double y;
        
        handle.read((char *) &x, sizeof(double));
        handle.read((char *) &y, sizeof(double));
        
        this->x.push_back(x);
        this->y.push_back(y);
    }
    
    handle.close();
}

// line_plot

line_plot :: line_plot(const char * title, const char * x_axis, const char * y_axis, vector <const char *> lines)
{
    this->descriptions.title = title;
    this->descriptions.axes.x = x_axis;
    this->descriptions.axes.y = y_axis;
    
    for(unsigned int i=0; i<lines.size(); i++)
        this->lines.push_back(line(lines[i]));
}

#endif