#ifndef EXPERIMENTS_UNIT_FIELD_VARIANCE_VS_CHARGES_VS_DISTANCE_H
#define EXPERIMENTS_UNIT_FIELD_VARIANCE_VS_CHARGES_VS_DISTANCE_H

#include <mutex>
#include <vector>
#include <iostream>
#include <stdio.h>

#include "../classes.h"
#include "experiment.h"
#include "../plot/line.h"
#include "../variance/box.h"

namespace experiments
{
    class unit_field_variance_vs_charges_vs_distance : public experiment
    {
        const char * path;
        
        struct
        {
            double edge;
            unsigned int max_charges;
            vector <double> distances;
            
            unsigned int tests;
        } settings;
        
        struct
        {
            const char * title;
            const char * x_axis;
            const char * y_axis;
            vector <const char *> lines;
        } descriptions;
        
        line_plot * plot;
        recursive_mutex plot_mutex;
        
    public:
        
        // Constructors
        
        unit_field_variance_vs_charges_vs_distance(const char *, double, unsigned int, vector <double>, unsigned int);
        
    private:
        
        // Private methods
        
        unsigned int setup();
        void work_unit(unsigned int);
        void finalize();
    };
};

#endif