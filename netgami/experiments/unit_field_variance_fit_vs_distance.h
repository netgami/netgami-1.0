#ifndef EXPERIMENTS_UNIT_FIELD_VARIANCE_FIT_VS_DISTANCE_H
#define EXPERIMENTS_UNIT_FIELD_VARIANCE_FIT_VS_DISTANCE_H

#include "../classes.h"
#include "experiment.h"
#include "../plot/line.h"
#include "../variance/box.h"
#include "../statistics/linear_fit.h"

namespace experiments
{
    class unit_field_variance_fit_vs_distance : public experiment
    {
        struct
        {
            const char * plot;
            const char * slope;
            const char * intercept;
        } paths;
        
        struct
        {
            double edge;
            
            struct
            {
                unsigned int max;
                unsigned int min;
            } charges;
            
            struct
            {
                double min;
                double max;
                double steps;
            } distance;
            
            unsigned int tests;
        } settings;
        
        struct
        {
            const char * title;
            const char * x_axis;
            const char * y_axis;
            vector <const char *> lines;
        } descriptions;
        
        line_plot * plot;
        recursive_mutex plot_mutex;
        
    public:
        
        // Constructors
        
        unit_field_variance_fit_vs_distance(const char *, const char *, const char *, double, unsigned int, unsigned int, double, double, unsigned int, unsigned int);
        
    private:
        
        // Private methods
        
        unsigned int setup();
        void work_unit(unsigned int);
        void finalize();

    };
}

#endif
