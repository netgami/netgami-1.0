#ifndef EXPERIMENTS_UNIT_FIELD_VARIANCE_VS_CHARGES_VS_DISTANCE_CPP
#define EXPERIMENTS_UNIT_FIELD_VARIANCE_VS_CHARGES_VS_DISTANCE_CPP

#include "unit_field_variance_vs_charges_vs_distance.h"
#include "experiment.cpp"
#include "../plot/line.cpp"
#include "../variance/box.cpp"

// Constructors

experiments :: unit_field_variance_vs_charges_vs_distance :: unit_field_variance_vs_charges_vs_distance(const char * path, double edge, unsigned int max_charges, vector <double> distances, unsigned int tests)
{
    this->path = path;
    
    this->settings.edge = edge;
    this->settings.max_charges = max_charges;
    this->settings.distances = distances;

    this->settings.tests = tests;
}

// Methods

unsigned int experiments :: unit_field_variance_vs_charges_vs_distance :: setup()
{
    this->descriptions.title = new char[256];
    sprintf((char *) this->descriptions.title, "Unit field variance (edge: %f)", this->settings.edge);
    
    this->descriptions.x_axis = new char[256];
    sprintf((char *) this->descriptions.x_axis, "Number of charges");
    
    this->descriptions.y_axis = new char[256];
    sprintf((char *) this->descriptions.y_axis, "Unit field variance");
    
    for(unsigned int i=0; i<this->settings.distances.size(); i++)
    {
        const char * line = new char[256];
        sprintf((char *) line, "Distance: %f", this->settings.distances[i]);
        
        this->descriptions.lines.push_back(line);
    }
    
    this->plot = new line_plot(this->descriptions.title, this->descriptions.x_axis, this->descriptions.y_axis, this->descriptions.lines);
    
    return (unsigned int) this->settings.distances.size() * this->settings.max_charges;
}

// Private methods

void experiments :: unit_field_variance_vs_charges_vs_distance :: work_unit(unsigned int work_unit)
{
    double distance = this->settings.distances[work_unit / this->settings.max_charges];

    unsigned int charges = work_unit % this->settings.max_charges + 1;
    
    variance_box variance_box(this->settings.edge, charges);
    
    double field_variance = 0;
    
    for(unsigned int i=0; i<this->settings.tests; i++)
        field_variance += variance_box.field_variance(distance) / charges;
    
    field_variance /= this->settings.tests;
    
    this->plot_mutex.lock();
    
    this->plot->lines[work_unit / this->settings.max_charges].add(charges, field_variance);
    
    this->plot_mutex.unlock();
}

void experiments :: unit_field_variance_vs_charges_vs_distance :: finalize()
{
    plotter.plot(this->path, *(this->plot), LINEAR, LOWER_RIGHT);
    
    delete [] this->descriptions.title;
    delete [] this->descriptions.x_axis;
    delete [] this->descriptions.y_axis;

    for(unsigned int i=0; i<this->descriptions.lines.size(); i++)
        delete [] this->descriptions.lines[i];
}

#endif