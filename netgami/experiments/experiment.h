#ifndef EXPERIMENTS_EXPERIMENT_H
#define EXPERIMENTS_EXPERIMENT_H

#include <thread>
#include <mutex>

#include "../classes.h"

using namespace std;

class experiment
{
    struct work_units : public recursive_mutex
    {
        unsigned int current;
        unsigned int count;
    };
    
    work_units work_units;
    
public:
    
    // Methods
    
    void run(unsigned int = thread::hardware_concurrency());
    
protected:
    
    // Private methods
    
    virtual unsigned int setup() = 0;
    virtual void work_unit(unsigned int) = 0;
    virtual void finalize() = 0;
    
    // Processing
    
    void thread();
};

#endif