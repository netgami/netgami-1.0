#ifndef EXPERIMENTS_UNIT_FIELD_VARIANCE_FIT_VS_DISTANCE_CPP
#define EXPERIMENTS_UNIT_FIELD_VARIANCE_FIT_VS_DISTANCE_CPP

#include "unit_field_variance_fit_vs_distance.h"
#include "experiment.cpp"
#include "../plot/line.cpp"
#include "../variance/box.cpp"
#include "../statistics/linear_fit.cpp"

// Constructors

experiments :: unit_field_variance_fit_vs_distance :: unit_field_variance_fit_vs_distance(const char * plot_path, const char * slope_path, const char * intercept_path, double edge, unsigned int min_charges, unsigned int max_charges, double min_distance, double max_distance, unsigned int distance_steps, unsigned int tests)
{
    this->paths.plot = plot_path;
    this->paths.slope = slope_path;
    this->paths.intercept = intercept_path;
    
    this->settings.edge = edge;
    this->settings.charges.max = max_charges;
    this->settings.charges.min = min_charges;
    this->settings.distance.max = max_distance;
    this->settings.distance.min = min_distance;
    this->settings.distance.steps = distance_steps;
    
    this->settings.tests = tests;
}

// Methods

unsigned int experiments :: unit_field_variance_fit_vs_distance :: setup()
{
    this->descriptions.title = new char[256];
    sprintf((char *) this->descriptions.title, "Unit field variance (edge: %f)", this->settings.edge);
    
    this->descriptions.x_axis = new char[256];
    sprintf((char *) this->descriptions.x_axis, "Distance");
    
    this->descriptions.y_axis = new char[256];
    sprintf((char *) this->descriptions.y_axis, "Fit");
    
    char * slope_line = new char[256];
    sprintf(slope_line, "Slope");
    
    char * intercept_line = new char[256];
    sprintf(intercept_line, "Intercept");
    
    this->descriptions.lines.push_back(slope_line);
    this->descriptions.lines.push_back(intercept_line);
    
    this->plot = new line_plot(this->descriptions.title, this->descriptions.x_axis, this->descriptions.y_axis, this->descriptions.lines);
    
    return (this->settings.distance.steps + 1);
}

// Private methods

void experiments :: unit_field_variance_fit_vs_distance :: work_unit(unsigned int work_unit)
{
    double distance = this->settings.distance.min + (this->settings.distance.max - this->settings.distance.min) * work_unit / this->settings.distance.steps;
    
    linear_fit fit;
    
    for(unsigned int charges = this->settings.charges.min; charges < this->settings.charges.max; charges++)
    {
        variance_box variance_box(this->settings.edge, charges);
        double field_variance = 0;
        
        for (unsigned int i = 0; i < this->settings.tests; i++)
            field_variance += variance_box.field_variance(distance);
        
        field_variance /= this->settings.tests;
        
        fit.add(charges, field_variance/charges);
    }
    
    struct linear_fit :: fit parameters = fit.fit();
    
    this->plot_mutex.lock();
    
    this->plot->lines[0].add(distance, parameters.slope);
    this->plot->lines[1].add(distance, parameters.intercept);
    
    this->plot_mutex.unlock();
}

void experiments :: unit_field_variance_fit_vs_distance :: finalize()
{
    this->plot->lines[0].save(this->paths.slope);
    this->plot->lines[1].save(this->paths.intercept);
    
    plotter.plot(this->paths.plot, *(this->plot), SEMILOGY);
    
    delete [] this->descriptions.title;
    delete [] this->descriptions.x_axis;
    delete [] this->descriptions.y_axis;
    
    for(unsigned int i=0; i<this->descriptions.lines.size(); i++)
        delete [] this->descriptions.lines[i];
}


#endif
