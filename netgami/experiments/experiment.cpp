#ifndef EXPERIMENTS_EXPERIMENT_CPP
#define EXPERIMENTS_EXPERIMENT_CPP

#include "experiment.h"

// Methods

void experiment :: run(unsigned int threads_count)
{
    this->work_units.current = 0;
    this->work_units.count = this->setup();
    
    cout<<"Work units count: "<<this->work_units.count<<endl;
    
    class thread ** threads = new class thread * [threads_count];
    
    for(unsigned int i=0; i<threads_count; i++)
        threads[i] = new class thread(&experiment :: thread, this);
    
    for(unsigned int i=0; i<threads_count; i++)
    {
        threads[i]->join();
        delete threads[i];
    }
    
    delete threads;
     
    this->finalize();
}

// Processing

void experiment :: thread()
{
    vec3 :: random :: enable();
    
    while(true)
    {
        unsigned int work_unit;
        
        this->work_units.lock();
        work_unit = this->work_units.current++;

        if(work_unit < this->work_units.count)
            cout<<"Processing work unit "<<work_unit<<endl;

        this->work_units.unlock();
        
        if(work_unit < this->work_units.count)
            this->work_unit(work_unit);
        else
            break;
    }
    
    vec3 :: random :: disable();
}

#endif