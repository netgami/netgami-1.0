#ifndef VARIANCE_BOX_CPP
#define VARIANCE_BOX_CPP

#include "box.h"
#include "../fields/electrostatic3.cpp"
#include "../fields/power3.cpp"

// Constructors

variance_box :: variance_box(double edge, unsigned int charges_count)
{
    this->edge = edge;
    
    this->charges.count = charges_count;
    this->charges.positions = new vec[this->charges.count];
}

// Destructor

variance_box :: ~variance_box()
{
    delete [] this->charges.positions;
}

// Methods

double variance_box :: field_variance(double radius)
{
    return this->field_variance(vec :: random :: uniform_polar(radius));
}

double variance_box :: field_variance(vec position)
{
    vec charge_center = vec :: null;
    
    for(unsigned int i=0; i<this->charges.count; i++)
    {
        this->charges.positions[i] = vec :: random :: uniform_box(this->edge);
        charge_center += this->charges.positions[i];
    }
    
    charge_center /= this->charges.count;
    
    vec center_field = this->field.field(position - charge_center, this->charges.count);
    
    vec exact_field = vec :: null;
    
    for(unsigned int i=0; i<this->charges.count; i++)
        exact_field += this->field.field(position - this->charges.positions[i]);
    
    return ~(exact_field - center_field);
}

#endif