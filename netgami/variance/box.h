#ifndef VARIANCE_BOX_H
#define VARIANCE_BOX_H

#include <random>
#include <math.h>

#include "../classes.h"
#include "../fields/electrostatic3.h"
#include "../fields/power3.h"

class variance_box
{
    double edge;
    
    struct
    {
        vec * positions;
        unsigned int count;
    } charges;
    
    field field;
    
public:
    
    // Constructors
    
    variance_box(double, unsigned int);
    
    // Destructor
    
    ~variance_box();
    
    // Methods
    
    double field_variance(double);
    double field_variance(vec);
};

#endif