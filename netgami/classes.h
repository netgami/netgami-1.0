#ifndef CLASSES_H
#define CLASSES_H

// Plot

class plotter;
class line_plot;

// Geometry

struct vec3;

// Fields

namespace fields
{
    class electrostatic3;
    class power3;
};

// Springs

namespace springs
{
    class hooke;
};

// Variance

// Statistics

class linear_fit;

// Engine

namespace node_extensions
{
    class charge;
    class fixed;
    
    template <typename... extensions> class iterator;
};

template <typename... extensions> class node;

namespace connectivity_extensions
{
    class stiffness;
    
    template <typename... extensions> class pack;
    template <typename... extensions> class iterator;
};

template <typename... extensions> class connectivity;

// Typedefs

typedef vec3 vec;
typedef fields :: electrostatic3 field;

#endif