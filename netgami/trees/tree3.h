#ifndef TREES_TREE3_H
#define TREES_TREE3_H

#include <iostream>

#include "../classes.h"
#include "../geometry/vec3.h"

using namespace std;

template <class B, class L> class tree3
{
    B ** branches;
    L * leaves;
    
    unsigned int layers;
    
public:
    
    struct settings
    {
        static unsigned long int max_nodes;
    };
    
    settings settings;
    
    // Constructors
    
    tree3();
    
    // Destructor
    
    ~tree3();
    
    // Methods
    
    void traverse(function <bool (B &)>, function <void (L &)>);
    void update(vec3, function <void (B &)>, function <void (L &)>);
    
private:
    
    // Private methods
    
    void traverse(unsigned int, unsigned long int, function <bool (B &)>, function <void (L &)>);
};

template <class B, class L> unsigned long int tree3 <B, L> :: settings :: max_nodes = 19173961;

#endif