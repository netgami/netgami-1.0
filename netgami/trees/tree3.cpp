#ifndef TREES_TREE3_CPP
#define TREES_TREE3_CPP

#include "tree3.h"
#include "../geometry/vec3.cpp"

// Constructors

template <class B, class L> tree3 <B, L> :: tree3()
{
    unsigned long int nodes_count = 0;
    this->layers = 0;
    
    for(this->layers = 0; nodes_count + (1 << (3 * this->layers)) <= this->settings.max_nodes; this->layers++)
        nodes_count += (1 << (3 * this->layers));
    
    this->branches = new B * [this->layers - 1];
    
    for(unsigned int i=0; i<this->layers - 1; i++)
        this->branches[i] = new B[1 << (3 * i)];
    
    this->leaves = new L[1 << (3 * (this->layers - 1))];
}

// Destructor

template <class B, class L> tree3 <B, L> :: ~tree3 ()
{
    for(unsigned int i=0; i<this->layers -1 ; i++)
        delete [] this->branches[i];
    
    delete [] this->branches;
    delete [] this->leaves;
}

// Methods

template <class B, class L> void tree3 <B, L> :: traverse(function <bool (B &)> branch_callback, function <void (L &)> leaf_callback)
{
    this->traverse(0, 0, branch_callback, leaf_callback);
}

template <class B, class L> void tree3 <B, L> :: update(vec3 position, function <void (B &)> branch_callback, function <void (L &)> leaf_callback)
{
    unsigned long int node = 0;
    
    for(unsigned int layer = 0; layer < this->layers - 1; layer++)
    {
        cout<<"Calling branch_callback on layer "<<layer<<" and node "<<node<<endl;
        branch_callback(this->branches[layer][node]);

        unsigned int shift = (position.x > 0.5 ? 0x1 : 0x0) | (position.y > 0.5 ? 0x2 : 0x0) | (position.z > 0.5 ? 0x4 : 0x0);
        
        node = node * 8 + shift;
        position = vec3(fmod(position.x * 2., 1.0), fmod(position.y * 2., 1.0), fmod(position.z * 2., 1.0));
    }

    cout<<"Calling leaf_callback on node "<<node<<endl;
    leaf_callback(this->leaves[node]);
}

// Private methods

template <class B, class L> void tree3 <B, L> :: traverse(unsigned int layer, unsigned long int node, function <bool (B &)> branch_callback, function <void (L &)> leaf_callback)
{
    if(layer < this->layers - 1)
    {
        if(branch_callback(this->branches[layer][node]))
            for(unsigned int i=0; i<8; i++)
            this->traverse(layer + 1, 8 * node + i, branch_callback, leaf_callback);
    }
    else
        leaf_callback(this->leaves[node]);
}

#endif